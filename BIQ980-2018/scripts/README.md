
To compile BIQ980-2018_LASTNAME.Rmd to a PDF file in `rstudio` (with the 
`knitr` button)) you need to generate the RData and figure files by running 
the prepared  R scripts first.

First re-install platexpress:

```r
## first re-install platexpress
library(devtools)
install_github("raim/platexpress")
```

Second, run the following scripts in a console or via `rstudio`:

```{bash}
R --vanilla < scripts/2018_parse.R # parses and collects all data
R --vanilla < scripts/2018_analysis.R # calls dpseg segmentation for topA data
R --vanilla < scripts/2018_figures.R # generates all figures
```
