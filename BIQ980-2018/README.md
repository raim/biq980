
To compile BIQ980-2018.Rmd to a PDF file in `rstudio` (or with `knitr`) you need to run the lesson 
scripts first, in `rstudio` or on a console:

```{bash}
R --vanilla < lessons/lesson_01_diurnal.R
R --vanilla < lessons/lesson_01_exponential.R

## install required packages:
sudo R --vanilla < lessons/installPackages.R

R --vanilla < lessons/lesson_02_platedata.R
R --vanilla < lessons/lesson_02_growthrates.R

R --vanilla < lessons/lesson_02_biolector.R
```
