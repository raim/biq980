
## Growth is affected by topA Overexpression

We grew all strains in M9ac medium with 0.4\\% glucose in microtiter plates, incubated in a Clariostar or  BioLector platereader at 37°C, in the presence of increasing concentrations of the inducer anhydro-tetracycline (aTc). 
Biomass development was measured every 10 min for 24 hours, using the optical density OD (Clariostar) or the back-scatter (BioLector) of light at 600 nm wavelength.

The resulting growth curves show clear effects of the aTc-induced overexpression of topA  (Fig. \ref{fig:biomass}a) but not of mVenus (Fig. \ref{fig:biomass}b). \textcolor{red}{Expand from here, where is the effect seen, what is the difference, what happened in groupII biolector and groupII Clariostar, what did the other strains (snoopy, EVC)? ...}


```{r, echo = FALSE, out.width=c("45%","45%","9%"), include=TRUE, ,fig.cap="\\label{fig:biomass}Growth measurements in microtiter platereaders of \\textit{E. coli} strains  plO3\\_topA (a) and plO3\\_mVenus (b), grown with varying concentrations of anhydro-tetracycline (aTc), see color legend in (c). Each line is the mean of four replicates and the transparent ranges indicate the 95\\% confidence interval of this mean. Biomass was measured as OD (Clariostar) or back-scatter (BioLector) at 600 nm every 10 minutes over 24 h.", fig.subcap = c('plO3\\_topA', 'plO3\\_mVenus', 'legend'), fig.show='hold'}
knitr::include_graphics(c("results/coarse/biomass_plO3_topA.png",
                          "results/coarse/biomass_plO3_mVenus.png",
                          "results/coarse/inducer_legend.png"))
```

### Effect of topA Overexpression on Growth Rates & Phases

To compare effects of gradual topA overexpression more quantitatively and independent of differences in initial cell density, we next used an in-house dynamic programing algorithm that minimizes the variance of residuals of the local linear regressions. This algorithm splits the growth curve into linear segments of the logarithm of the biomass measure (Fig. \ref{fig:rates}a). Each segment has a slope corresponding to the "local" growth rate. We then interpolated the growth rates to the biomass measurements (see Appendix Fig. \ref{appfig:rates}) using the function `interpolatePlateData` of `R` package `platexpress`, and sampled growth rates at a certain OD or back-scatter values (function `boxData` of `platexpress`), ie. at comparable points of each invidiual growth curve. Dose-response curves quantify the effect of aTc, and the results are quite consistent between experiments and platereaders. In three of the four experiments, the growth rates at OD or biomass measurements of 0.5 and 1.2 decreased with increasing induction of topA (Fig. \ref{fig:rates}b). \textcolor{red}{Expand from here, describe details ("growth rate decreased from X to Y"), where does above statement not fit, ...}.

 

```{r, echo = FALSE, out.width=c("33%"), include=TRUE, ,fig.cap="\\label{fig:rates} Growth curves where split into log-linear segments using a dynamic programing algorithm (a). Growth rates, ie. slopes of the linear segments, at 0.5 (b) or 1.2 (c) of the biomass measurement (OD or back-scatter; the error bars show the full range of values and the line connects median values of all replicates at each aTc concentration. \\textcolor{red}{TODO: use SD as bartype; fix x-axis label: correct name and units of substance'}", fig.subcap = c(''), fig.show='hold'}
knitr::include_graphics(c("results/coarse/segments_heatmap.png",
                          "results/coarse/segments_rates_at_biomass_05.png",
                          "results/coarse/segments_rates_at_biomass_12.png"))
```

\clearpage 

### Metabolic Effects of topA Overexpression

\textcolor{red}{TODO: Oxygen Consumption and pH Changes in the BioLector}


\clearpage

## Fluorescent Protein Expression vs. Cell Autofluorescence

We measured culture fluorescence at three different combinations of excitation and wavelengths, 513/553 nm and 497/540 nm (Clariostar) or 488/520 nm (BioLector). The first of these (513/553 nm), only available for the Clariostar platereader, has very little contribution from cell auto-fluorescence. \textcolor{red}{A clear mVenus-specific signal is only seen in the mVenus strain experiment of group I mVenus. Expand from here, what else is seen? ....}. 


### Loss of Effects in the *Snoopy* Strain

\textcolor{red}{the snoopy strain has lost both its responsiveness to aTc and mVenus
expression; and the mVenus strain shows no signal in group II, both experiments, and
in the BioLector experiment of group I; in which figures do we see that? what could be the reason? Just pipetting errors or how else could these genes/plasmid be lost inspite of two selection antibiotics!?}

\textcolor{red}{TODO: EVC fluorescence looks different from others, how
could this happen?}

### Effects of Cell Morphology

It is well known that optical density (OD) measurements are linearly related to the actual cell number or biomass, if cell morphology is constant. Specifically cell length can show non-linear effects on  the measurements, and longer cells give a higher OD measurement than expected by cell number or biomass [@Stevenson2016]. We had previously observed by microscopy of culture samples that overexpression of topA  induces length growth in a sub-population of cells, and the number of cells growing in length without division increased with increaseing induction strength (data not shown). Thus, part of the signal we obtain, especially the final OD/back-scatter in stationary phase, may be due to longer cells at less cell numbers or biomass.  This effect would require further clarification.

\textcolor{red}{Compare OD/back-scatter to auto-fluorescence per OD/back-scatter. Do we see differences? Eg. check out auto-fluorescence per biomass for the topA overexpression strain. What could those mean, given that we know already
that part of the biomass/OD signal is length growth?}


```{r, echo = FALSE, out.width=c("33%","33%","33%"), include=TRUE, ,fig.cap="\\label{fig:fluorescence_per_biomass} Cell auto-fluorescence and reporter gene fluorescence were measured at three different excitation/emission wavelength pairs, and each fluorescence time-series was normalized to biomass, ie. dividided by the respective biomass time-series (OD or back-scatter).", fig.subcap = c('513/553 nm','497/540 nm','488/520 nm'), fig.show='hold'}
knitr::include_graphics(rev(c("results/coarse/fluorescence_auto_per_scatter_groupI_biolector.png","results/coarse/fluorescence_auto_per_OD_groupI_clariostar.png","results/coarse/fluorescence_mVenus_per_OD_groupI_clariostar.png" )))
```

```{r, echo = FALSE, out.width=c("33%","33%","33%"), include=TRUE, ,fig.cap="\\label{fig:fluorescence_II} Same as Figure \\ref{fig:fluorescence} but for group II.", fig.subcap = c('513/553 nm','497/540 nm','488/520 nm'), fig.show='hold'}
knitr::include_graphics(rev(c("results/coarse/fluorescence_auto_per_scatter_groupII_biolector.png","results/coarse/fluorescence_auto_per_OD_groupII_clariostar.png","results/coarse/fluorescence_mVenus_per_OD_groupII_clariostar.png" )))
```


## An ODE Model captures the aTc Effect on Growth

\textcolor{red}{TODO: fit data via R package "growthrates" and "growthmodels"
to an Monod or other motdel. Or describe your results from your session with Nima at QTB here.}

